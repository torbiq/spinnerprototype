﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    #region Serialized links
    /// <summary>
    /// Exit button.
    /// </summary>
    [SerializeField]
    private Button _exitButton;
    /// <summary>
    /// Rpm displaying text.
    /// </summary>
    [SerializeField]
    private Text _rpmText;
    /// <summary>
    /// Coins text.
    /// </summary>
    [SerializeField]
    private Text _coinsText;
    /// <summary>
    /// Text containing spins count.
    /// </summary>
    [SerializeField]
    private Text _spinsText;
    /// <summary>
    /// Text containing best spins count.
    /// </summary>
    [SerializeField]
    private Text _bestSpinsText;
    /// <summary>
    /// Spins bar.
    /// </summary>
    [SerializeField]
    private Transform _barTransform;
    /// <summary>
    /// Spinner controller.
    /// </summary>
    [SerializeField]
    private SpinnerController _spinnerController;
    #endregion

    #region Private members
    /// <summary>
    /// Previous mouse position.
    /// </summary>
    private Vector2 _prevMousePos;
    /// <summary>
    /// Current mouse position.
    /// </summary>
    private Vector2 _currMousePos;
    /// <summary>
    /// Current drag time.
    /// </summary>
    private float _currentDragTime = 0;
    /// <summary>
    /// Maximal drag time.
    /// </summary>
    private float _maxDragTime = 0.6f;
    /// <summary>
    /// Is player holding spinner.
    /// </summary>
    private bool _isHoldingSpinner;
    /// <summary>
    /// Last finger speed.
    /// </summary>
    private float _lastAngularVelocity;
    /// <summary>
    /// Drag time that will stop spinner.
    /// </summary>
    private float _dragTimeToStop = 1.5f;
    /// <summary>
    /// Max spins count.
    /// </summary>
    private int _neededSpinsCount = 25;
    /// <summary>
    /// Spins record.
    /// </summary>
    private int _spinsRecord = 0;
    /// <summary>
    /// Min angular velocity to spin.
    /// </summary>
    private float _minAngularVelocity = 5.0f;
    #endregion

    public int BestSpins {
        get {
            return _spinsRecord;
        }
        set {
            _spinsRecord = value;
            _bestSpinsText.text = "BEST: " + _spinsRecord.ToString();
        }
    }

    private void Awake() {
        _exitButton.onClick.AddListener(Application.Quit);
        _spinnerController.OnSpinningEnded += OnSpinnerStoppedEventHandler;
    }

    private float CalculateAngleBetweenPositions(Vector3 center, Vector3 dotB, Vector3 dotC) {
        // This version doesn't works with negative values.
        Vector3 vecB = (center - dotC);
        Vector3 vecC = (center - dotB);

        // Angles in degrees
        float angleB = Mathf.Atan2(vecB.y, vecB.x);
        float angleC = Mathf.Atan2(vecC.y, vecC.x);

        // Return angle in degrees
        return (angleC - angleB) * Mathf.Rad2Deg;
    }

    private void OnSpinnerStoppedEventHandler(int spinsCount) {
        if (BestSpins < spinsCount) {
            BestSpins = spinsCount;
        }
    }

    // Update is called once per frame
    private void Update () {
        bool mouseDown = Input.GetMouseButtonDown(0);
        bool mouseDrag = Input.GetMouseButton(0);
        bool mouseUp = Input.GetMouseButtonUp(0);

        Vector3 scale = _barTransform.localScale;
        scale.x = Mathf.Min(_spinnerController.SpinsMadeSinceLastVelocityChange / _neededSpinsCount, 1.0f);
        _barTransform.localScale = scale;

        if (mouseDown) {
            _currMousePos = Input.mousePosition;
            var touchedCollider = Physics2D.OverlapPoint(_currMousePos);
            if (touchedCollider == _spinnerController.collider2d) {
                _currentDragTime = 0.0f;
                _isHoldingSpinner = true;
            }
        }
        if (mouseDrag && _isHoldingSpinner) {

            _currentDragTime += Time.deltaTime;

            if (_currentDragTime < _dragTimeToStop && _spinnerController.currentAngularVelocity > 0.0f) {
                return;
            }

            _prevMousePos = _currMousePos;
            _currMousePos = Input.mousePosition;
            
            float angularDelta = CalculateAngleBetweenPositions(_spinnerController.transform.position, _currMousePos, _prevMousePos);
            float linearSpeed = Mathf.Min((_currMousePos - _prevMousePos).magnitude / 6, 1000) * (angularDelta >= 0.0f ? 1.0f : -1.0f);
            _lastAngularVelocity = linearSpeed / Time.deltaTime;
            _spinnerController.ForceRotate(angularDelta);
        }
        if (mouseUp && _isHoldingSpinner) {
            if (_currentDragTime < _maxDragTime && Mathf.Abs(_lastAngularVelocity) > _minAngularVelocity) {
                // Remembering direction.
                float dir = _lastAngularVelocity > 0.0f ? 1.0f : -1.0f;
                // Trimming maximal start linear velocity.
                _spinnerController.ChangeVelocity(dir * Mathf.Max(_lastAngularVelocity, 500));
            }
            // If spinner was dragged too much or had too small velocity
            else {
                // Failed.
            }
            _isHoldingSpinner = false;
            _currentDragTime = 0.0f;
        }

        _rpmText.text = _spinnerController.RotesPerMinute.ToString();
        _spinsText.text = "SPINS: " + ((int)_spinnerController.SpinsMadeSinceLastVelocityChange).ToString();

    }
}
