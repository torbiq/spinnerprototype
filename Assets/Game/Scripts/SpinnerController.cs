﻿using UnityEngine;
using System.Collections;

public class SpinnerController : MonoBehaviour {

    public delegate void OnSpinningCallback(float angularSpeed);
    public delegate void OnSpinningEndCallback(int spinsCount);

    /// <summary>
    /// Called when you change velocity.
    /// </summary>
    public event OnSpinningCallback OnVelocityChanged;
    /// <summary>
    /// Called when spinner velocity comes to 0.
    /// </summary>
    public event OnSpinningEndCallback OnSpinningEnded;

    #region Serialized private members
    [SerializeField]
    private RectTransform _parentRectTransform;
    /// <summary>
    /// Spinner collider.
    /// </summary>
    [SerializeField]
    private Collider2D _collider2d;
    /// <summary>
    /// Friction amount.
    /// </summary>
    [SerializeField]
    private float _friction = 16.0f;
    /// <summary>
    /// Lerping speed.
    /// </summary>
    [SerializeField]
    private float _lerpingSpeed = 15.0f;
    #endregion

    #region Public members
    /// <summary>
    /// Current angular velocity.
    /// </summary>
    public float currentAngularVelocity { get; private set; }
    /// <summary>
    /// Rotation gained since last spin.
    /// </summary>
    public float rotationGainedSinceLastSpin { get; private set; } 
    /// <summary>
    /// Spins collider.
    /// </summary>
    public Collider2D collider2d {
        get {
            return _collider2d;
        }
    }
    /// <summary>
    /// Rotes per minute.
    /// </summary>
    public int RotesPerMinute {
        get {
            return (int)(Mathf.Abs(currentAngularVelocity) * 60.0f / 360.0f);
        }
    }
    /// <summary>
    /// Spins were made since last speed change.
    /// </summary>
    public float SpinsMadeSinceLastVelocityChange {
        get {
            return (Mathf.Abs(rotationGainedSinceLastSpin) / 360.0f);
        }
    }
    #endregion

    #region Public methods
    /// <summary>
    /// Basic speed applying script.
    /// </summary>
    /// <param name="velocity"></param>
    public void ChangeVelocity(float velocity) {
        // Resetting rotation gained.
        rotationGainedSinceLastSpin = 0.0f;
        // Receiving current velocity.
        currentAngularVelocity = velocity;
        // Calling velocity changing event.
        if (OnVelocityChanged != null) {
            OnVelocityChanged(velocity);
        }
        if (velocity == 0.0f) {
            if (OnSpinningEnded != null) {
                OnSpinningEnded((int)SpinsMadeSinceLastVelocityChange);
            }
        }
    }
    /// <summary>
    /// Forcing rotation (speeds sets to zero).
    /// </summary>
    public void ForceRotate(float rotationZ) {
        if (OnSpinningEnded != null) {
            OnSpinningEnded((int)SpinsMadeSinceLastVelocityChange);
        }
        // Resetting current angular velocity.
        currentAngularVelocity = 0.0f;
        // Rotating spinner around z coordinate.
        _parentRectTransform.Rotate(Vector3.forward, rotationZ);
    }
    #endregion

    #region Private methods
    /// <summary>
    /// Used to make update shorter.
    /// </summary>
    private void Rotate() {
        if (Mathf.Abs(currentAngularVelocity) > 0.0f) {
            // Direction of spin contained.
            int spinDir = currentAngularVelocity > 0.0f ? 1 : -1;
            // Applying friction to predicted velocity value.
            float predictedVelocity = spinDir * Mathf.Max(Mathf.Abs(currentAngularVelocity) - _friction * Time.deltaTime, 0.0f);
            // Lerping current velocity to predicted velocity.
            currentAngularVelocity = Mathf.Lerp(currentAngularVelocity, predictedVelocity, _lerpingSpeed * Time.deltaTime);
            // Calculate degree delta according current angular velocity.
            float rotationDelta = currentAngularVelocity * Time.deltaTime;
            // Rotation speed decreasing.
            rotationGainedSinceLastSpin += rotationDelta;
            // Rotating spinner around z coordinate.
            _parentRectTransform.Rotate(Vector3.forward, rotationDelta);
            // If speed quals zero - call event.
            if (currentAngularVelocity == 0.0f) {
                if (OnSpinningEnded != null) {
                    OnSpinningEnded((int)SpinsMadeSinceLastVelocityChange);
                }
            }
        }
    }
    private void Update() {
        Rotate();
    }
    #endregion
}
